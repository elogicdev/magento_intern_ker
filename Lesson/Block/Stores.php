<?php

declare(strict_types=1);

namespace Elogic\Lesson\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;
use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data\StoreInterface;
use Magento\Backend\Block\Template\Context;

class Stores extends Template
{
    /**
     * @var StoreRepositoryInterface
     */
    protected StoreRepositoryInterface $storeRepository;

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository,
        RequestInterface $request,
        array $data = []
    ) {
        $this->storeRepository = $storeRepository;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    /**
     * @return StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStores(): StoreInterface
    {
        $id = $this->request->getParam('id', null);

        return $this->storeRepository->getById((int) $id);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageUrl(): string
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}
