<?php

declare(strict_types=1);

namespace Elogic\Lesson\Block;

use Magento\Framework\View\Element\Template;
use Elogic\Lesson\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;
use Magento\Framework\UrlInterface;

class StoresList extends Template
{
    /**
     * @var StoreCollectionFactory
     */
    protected StoreCollectionFactory $collectionFactory;

    /**
     * @var Template\Context
     */
    private Template\Context $context;

    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;

    /**
     * @param Template\Context $context
     * @param StoreCollectionFactory $collectionFactory
     */
    public function __construct(
        Template\Context $context,
        StoreCollectionFactory $collectionFactory,
        UrlInterface $urlBuilder
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->context = $context;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return \Elogic\Lesson\Model\ResourceModel\Store\Collection
     */
    public function getStoresList()
    {
        return $this->collectionFactory->create();
    }

    /**
     * @return string
     */
    public function getStoreUrl(): string
    {
        return $this->urlBuilder->getUrl('stores/');
    }
}

