<?php

declare(strict_types=1);

namespace Elogic\Lesson\Plugin\Store;

use Elogic\Lesson\Api\Data\StoreInterfaceFactory;
use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data\StoreInterface;
use Elogic\Lesson\Helper\Data;
use Magento\Framework\Serialize\Serializer\Json;

class FindLocationPlugin
{
    /**
     * @var StoreInterfaceFactory
     */
    protected StoreInterfaceFactory $storeInterfaceFactory;

    /**
     * @var Data
     */
    protected Data $helperData;

    /**
     *
     * @var Json
     */
    protected Json $_jsonSerializer;

    /**
     * @param Data $helperData
     * @param Json $jsonSerializer
     * @param StoreInterfaceFactory $storeInterfaceFactory
     */
    public function __construct(
        Data $helperData,
        Json $jsonSerializer,
        StoreInterfaceFactory $storeInterfaceFactory
    ) {
        $this->helperData = $helperData;
        $this->_jsonSerializer = $jsonSerializer;
        $this->storeInterfaceFactory = $storeInterfaceFactory;
    }

    /**
     * @param StoreRepositoryInterface $subject
     * @param StoreInterface $store
     * @return void
     */
    public function beforeSave(StoreRepositoryInterface $subject, StoreInterface $store)
    {
        $lng = $store->getLongitude() ?? 0;
        $lat = $store->getLatitude() ?? 0;

        if (!$lng || !$lat) {
            $mapApi = $this->helperData->getGeneralConfig('api_key');
            $address = $store->getAddress();
            $address = str_replace(' ', '+', $address);

            try {
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$mapApi";
                // send api request
                $geocode = file_get_contents($url);
                $json = $this->_jsonSerializer->unserialize($geocode);
                //var_dump($json);
                if($json['status'] == 'OK') {
                    $latitude = $json['results'][0]['geometry']['location']['lat'];
                    $longitude = $json['results'][0]['geometry']['location']['lng'];

                    $store->setLongitude((float)$longitude);
                    $store->setLatitude((float)$latitude);
                }
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
        }
    }
}
