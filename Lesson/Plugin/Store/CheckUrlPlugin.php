<?php

declare(strict_types=1);

namespace Elogic\Lesson\Plugin\Store;

use Elogic\Lesson\Api\Data\StoreInterfaceFactory;
use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data\StoreInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class CheckUrlPlugin
{
    /**
     * @var StoreInterfaceFactory
     */
    protected StoreInterfaceFactory $storeInterfaceFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    protected SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param StoreInterfaceFactory $storeInterfaceFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoreInterfaceFactory $storeInterfaceFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->storeInterfaceFactory = $storeInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param StoreRepositoryInterface $subject
     * @param StoreInterface $store
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSave(StoreRepositoryInterface $subject, StoreInterface $store)
    {
        $url = $store->getUrlKey();
        $id = $store->getStoreId();

        $search_criteria = $this->searchCriteriaBuilder->addFilter('url_key', $url, 'eq')->create();
        $items = $subject->getList($search_criteria);
        $num = $items->getTotalCount();

        if ($num > 0) {
            $item = $items->getItems();
            $find_id = $item[0]['id'];

            if ($id != $find_id) {
                $i = 1;
                $newurl = $url . $i;

                while (true) {
                    $search_criteria = $this->searchCriteriaBuilder->addFilter('url_key', $newurl, 'eq')->create();
                    $items = $subject->getList($search_criteria);
                    $num = $items->getTotalCount();

                    if ($num > 0) {
                        $item = $items->getItems();
                        $find_id = $item[0]['id'];

                        if ($id != $find_id) {
                            $i++;
                            $newurl = $url . $i;
                        } else {
                            $store->setUrlKey($newurl);
                            break;
                        }
                    } else {
                        $store->setUrlKey($newurl);
                        break;
                    }
                }
            }
        }
    }
}
