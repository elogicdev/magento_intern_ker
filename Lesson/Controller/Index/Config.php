<?php

namespace Elogic\Lesson\Controller\Index;

use Elogic\Lesson\Helper\Data;


class Config extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Data
     */
    protected Data $helperData;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Elogic\Lesson\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {
        echo $this->helperData->getGeneralConfig('enable');
        exit();
    }
}
