<?php

declare(strict_types=1);

namespace Elogic\Lesson\Controller\Router;

use Elogic\Lesson\Api\StoreRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ResponseInterface;


/**
 * Class CustomRouter
 *
 * @package Elogic\Lesson\Controller
 */
class CustomRouter implements RouterInterface
{
    /**
     * @var StoreRepositoryInterface
     */
    protected StoreRepositoryInterface $storeRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var ActionFactory
     */
    protected ActionFactory $actionFactory;

    /**
     * @var ResponseInterface
     */
    protected ResponseInterface $_response;

    /**
     * @param StoreRepositoryInterface $storeRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        StoreRepositoryInterface $storeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,

        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->storeRepository = $storeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');
        $paramsArr = explode('/', $identifier);

        if ($paramsArr[0] == 'stores' && count($paramsArr) > 1) {
            $url = $paramsArr[1];

            $search_criteria = $this->searchCriteriaBuilder->addFilter('url_key', $url, 'eq')->create();
            $items = $this->storeRepository->getList($search_criteria);
            $num = $items->getTotalCount();

            if ($num == 0) {
                return null;
            } else {
                $item = $items->getItems();
                $id = $item[0]['id'];
                $request->setModuleName('elogic_lesson')->setControllerName('index')->setActionName('stores');
                $request->setParams([
                    'id' => $id,
                ]);

                return $this->actionFactory->create(Forward::class, ['request' => $request]);
            }
        }
        if ($paramsArr[0] == 'stores' && count($paramsArr) == 1) {
            $request->setModuleName('elogic_lesson')->setControllerName('index')->setActionName('index');

            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        return null;
    }
}
