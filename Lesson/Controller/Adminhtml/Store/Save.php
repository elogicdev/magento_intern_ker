<?php

declare(strict_types=1);

namespace Elogic\Lesson\Controller\Adminhtml\Store;

use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data\StoreInterfaceFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\Result;


class Save extends Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected DataPersistorInterface $dataPersistor;

    /**
     * @var StoreInterfaceFactory $storeFactory
     */
    private StoreInterfaceFactory $storeFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private StoreRepositoryInterface $storeRepository;

    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;

    /**
     * @var UploaderFactory
     */
    protected UploaderFactory $uploaderFactory;

    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected Filesystem\Directory\WriteInterface $mediaDirectory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreInterfaceFactory $storeFactory
     * @param UploaderFactory $uploaderFactory
     * @param Filesystem $filesystem
     * @param Validator $formKeyValidator
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        StoreRepositoryInterface $storeRepository,
        StoreInterfaceFactory $storeFactory,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        Validator $formKeyValidator
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->dataPersistor = $dataPersistor;
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    /**
     * @return Result\Redirect
     * @throws \Exception
     */
    public function execute(): Result\Redirect
    {
        $resultPageFactory = $this->resultRedirectFactory->create();
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $this->messageManager->addErrorMessage(__("Form key is Invalidate"));
            return $resultPageFactory->setPath('*/*/index');
        }
        $data = $this->getRequest()->getPostValue();
        $fileUploader = null;

        if ($data) {
            $model = $this->storeFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->storeRepository->getById((int) $id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This block no longer exists.'));
                    return $resultPageFactory->setPath('*/*/');
                }
            }
            $model->setData($data);
            $params = $this->getRequest()->getParams();

            $imageId = 'image';

            if (isset($params['image']) && count($params['image']) && isset($params['image'][0]['tmp_name'])) {
                $imageId = $params['image'][0];
                if (!file_exists($imageId['tmp_name'])) {
                    $imageId['tmp_name'] = $imageId['path'] . '/' . $imageId['file'];
                }

                $fileUploader = $this->uploaderFactory->create(['fileId' => $imageId]);
                $fileUploader->setAllowedExtensions(['jpg', 'jpeg', 'png']);
                $fileUploader->setAllowRenameFiles(true);
                $fileUploader->setAllowCreateFolders(true);
                $fileUploader->validateFile();

                //upload image
                $info = $fileUploader->save($this->mediaDirectory->getAbsolutePath('lesson/store'));
                $model->setImage($this->mediaDirectory->getRelativePath('lesson/store') . '/' . $info['file']);
            } elseif (isset($params['image']) ) {
                $model->unsetData('image');
            } else {
                $model->setData('image',null);
            }

            $name = $model->getName();
            $url = preg_replace('#[^0-9a-z]+#i', '-', $name);
            $url = strtolower($url);

            $model ->setUrlKey($url);

            try {
                $this->storeRepository->save($model);
                $this->messageManager->addSuccessMessage(__("Data Saved Successfully."));
                $this->dataPersistor->clear('elogic_lesson_store');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the store.'));
            }

            $buttondata = $this->getRequest()->getParam('back');
            if ($buttondata == 'add') {
                return $resultPageFactory->setPath('*/*/new');
            }
            if ($buttondata == 'close') {
                return $resultPageFactory->setPath('*/*/index');
            }

            $this->dataPersistor->set('elogic_lesson_store', $data);
            $id = $model->getId();
            return $resultPageFactory->setPath('*/*/new', ['id' => $id]);
        }

        return $resultPageFactory->setPath('*/*/index');
   }
}
