<?php

declare(strict_types=1);

namespace Elogic\Lesson\Controller\Adminhtml\Store;

use Elogic\Lesson\Model\ResourceModel\Store\CollectionFactory;
use Elogic\Lesson\Api\StoreRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\Result;


class MassDelete extends Action
{
    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $storeFactory;

    /**
     * @var Filter
     */
    protected Filter $filter;

    /**
     * @var StoreRepositoryInterface
     */
    private StoreRepositoryInterface $storeRepository;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Filter $filter
     * @param CollectionFactory $storeFactory
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Filter $filter,
        CollectionFactory $storeFactory,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->storeFactory = $storeFactory;
        $this->filter = $filter;
        $this->storeRepository = $storeRepository;
        parent::__construct($context);
    }

    /**
     * @return Result\Redirect
     * @throws LocalizedException
     */
    public function execute(): Result\Redirect
    {
        $collection = $this->filter->getCollection($this->storeFactory->create());

        $productDeleted = 0;
        $productDeletedError = 0;
        foreach ($collection->getItems() as $store) {
            try {
                $this->storeRepository->deleteById((int) $store['id']);
                $productDeleted++;
            } catch (LocalizedException $exception) {
                $productDeletedError++;
            }
        }

        if ($productDeleted) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $productDeleted)
            );
        }

        if ($productDeletedError) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted.',
                    $productDeletedError
                )
            );
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index');
    }
}
