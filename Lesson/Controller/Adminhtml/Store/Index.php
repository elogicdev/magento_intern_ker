<?php

declare(strict_types=1);

namespace Elogic\Lesson\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\Page;


class Index extends Action
{
    const ADMIN_RESOURCE = 'Elogic_Lesson::Store';

    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Lesson::Store');
        $resultPage->addBreadcrumb(__('Store Data'), __('Store Data'));
        $resultPage->addBreadcrumb(__('Store Data'), __('Store Data'));
        $resultPage->getConfig()->getTitle()->prepend(__('Store Data'));

        return $resultPage;
    }
}
