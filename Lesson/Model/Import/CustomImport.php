<?php

declare(strict_types=1);

namespace Elogic\Lesson\Model\Import;

use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data\StoreInterfaceFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\Framework\Exception\LocalizedException;


class CustomImport extends AbstractEntity
{
    const ID          = 'id';
    const NAME        = 'name';
    const DESC        = 'description';
    const ADDRESS     = 'address';
    const SCHEDULE    = 'schedule';
    const TABLE       = 'elogic_lesson_store';
    const ENTITY_CODE = 'custom_import';

    /**
     * @var bool
     */
    protected $needColumnCheck = true;

    /**
     * @var bool
     */
    protected $logInHistory = true;

    /**
     * @var string[]
     */
    protected $_permanentAttributes = [self::ID];

    /**
     * @var string[]
     */
    protected $validColumnNames = [self::ID, self::NAME, self::DESC, self::ADDRESS, self::SCHEDULE,
        'country', 'city', 'zip', 'position', 'state', 'phone', 'category', 'actions_serialized', 'stores',
        'marker_img', 'show_schedule', 'url_key', 'meta_title', 'meta_description', 'meta_robots', 'short_description',
        'canonical_url', 'store_img',];

    /**
     * @var AdapterInterface
     */
    protected $_connection;

    /**
     * @var ResourceConnection
     */
    protected ResourceConnection $_resource;

    /**
     * @var StoreInterfaceFactory $storeFactory
     */
    private StoreInterfaceFactory $storeFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private StoreRepositoryInterface $storeRepository;

    /**
     * @param JsonHelper $jsonHelper
     * @param ImportHelper $importExportData
     * @param Data $importData
     * @param ResourceConnection $resource
     * @param Helper $resourceHelper
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreInterfaceFactory $storeFactory
     */
    public function __construct(
        JsonHelper $jsonHelper,
        ImportHelper $importExportData,
        Data $importData,
        ResourceConnection $resource,
        Helper $resourceHelper,
        ProcessingErrorAggregatorInterface $errorAggregator,
        StoreRepositoryInterface $storeRepository,
        StoreInterfaceFactory $storeFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->initMessageTemplates();
    }

    /**
     * @return string[]
     */
    public function getValidColumnNames(): array
    {
        return $this->validColumnNames;
    }

    /**
     * @return string
     */
    public function getEntityTypeCode(): string
    {
        return static::ENTITY_CODE;
    }

    /**
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum): bool
    {
        $name = $rowData['name'] ?? '';
        $address = $rowData['address'] ?? '';

        if(!$name) {
            $this->addRowError('NameIsRequired', $rowNum);
        }

        if(!$address) {
            $this->addRowError('AddressIsRequired', $rowNum);
        }

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * @return void
     */
    private function initMessageTemplates() {
        $this->addMessageTemplate('NameIsRequired', __('The name cannot be empty'));
        $this->addMessageTemplate('AddressIsRequired', __('The address cannot be empty'));
        $this->addMessageTemplate('SavingError', __('Save Error'));
    }

    /**
     * @return bool
     */
    protected function _importData(): bool
    {
        switch ($this->getBehavior()) {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
                break;
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
                break;
            case Import::BEHAVIOR_APPEND:
                $this->saveAndReplaceEntity();
                break;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId= $rowData[static::ID];
                    $rows [] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows) {
            $this->deleteEntityFinish(array_unique($rows));
        }

        return false;
    }

    /**
     * @return void
     */
    protected function saveAndReplaceEntity(): void
    {
        $behavior = $this->getBehavior();
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowId = $rowData[self::ID];
                $rows[] = $rowId;

                $address = $rowData[static::ADDRESS] . ', ' . $rowData['city'] . ', ' . $rowData['country'];
                $entityList[$rowId][] = [
                    static::NAME     => $rowData[static::NAME],
                    static::DESC     => $rowData[static::DESC],
                    static::ADDRESS  => $address,
                    static::SCHEDULE => $rowData[static::SCHEDULE],
                ];
            }

            if (Import::BEHAVIOR_REPLACE == $behavior) {
                if ($rows) {
                    if ($this->deleteEntityFinish(array_unique($rows))) {
                        $this->saveEntityFinish($entityList);
                    }
                }
            } elseif (Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList);
            }
        }
    }

    /**
     * @param array $entityData
     * @return bool
     */
    protected function saveEntityFinish(array $entityData): bool
    {
        if ($entityData) {
            $rows = [];
            foreach ($entityData as $entityRows) {
                foreach ($entityRows as $row) {
                    $rows[] = $row;
                }
            }
            if ($rows) {
                foreach ($rows as $data) {
                    $model = $this->storeFactory->create();

                    $model->setData('name',$data[static::NAME]);
                    $model->setData('description',$data[static::DESC]);
                    $model->setData('address',$data[static::ADDRESS]);
                    $model->setData('schedule',$data[static::SCHEDULE]);

                    $name = $model->getName();

                    $url = preg_replace('#[^0-9a-z]+#i', '-', $name);
                    $url = strtolower($url);

                    $model ->setUrlKey($url);

                    try {
                        $this->storeRepository->save($model);
                    } catch (LocalizedException $e) {
                        $this->addMessageTemplate('SavingError', __('Save Error'));
                    }
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param array $entityIds
     * @return bool
     */
    protected function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName(static::TABLE),
                    $this->_connection->quoteInto(static::ID . ' IN (?)', $entityIds));

                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }
}
