<?php

declare(strict_types=1);

namespace Elogic\Lesson\Model;

use Elogic\Lesson\Api\StoreRepositoryInterface;
use Elogic\Lesson\Api\Data;
use Elogic\Lesson\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;
use Elogic\Lesson\Model\ResourceModel\Store as ResourceStore;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class StoreRepository implements StoreRepositoryInterface
{
    /**
     * @var ResourceStore
     */
    protected ResourceStore $resource;
    /**
     * @var StoreCollectionFactory $storeCollectionFactory
     */
    private StoreCollectionFactory $storeCollectionFactory;

    /**
     * @var Data\StoreSearchResultsInterfaceFactory $searchResultsFactory
     */
    private Data\StoreSearchResultsInterfaceFactory $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface $collectionProcessor
     **/
    private CollectionProcessorInterface $collectionProcessor;

    /**
     * @var Data\StoreInterfaceFactory $storeDataFactory
     */
    private Data\StoreInterfaceFactory $storeDataFactory;

    /**
     * @param ResourceStore $resource
     * @param StoreCollectionFactory $storeCollectionFactory
     * @param Data\StoreSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\StoreInterfaceFactory $storeDataFactory
     */
    public function __construct(
        ResourceStore $resource,
        StoreCollectionFactory $storeCollectionFactory,
        Data\StoreSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\StoreInterfaceFactory $storeDataFactory
    ) {
        $this->resource = $resource;
        $this->collectionProcessor = $collectionProcessor;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->storeDataFactory = $storeDataFactory;
    }

    /**
     * @inheritDoc
     * @throws CouldNotSaveException
     */
    public function save(Data\StoreInterface $store): Data\StoreInterface
    {
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $store;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getById(int $id): Data\StoreInterface
    {
        $store = $this->storeDataFactory->create();
        $this->resource->load($store, $id);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('The CMS block with the "%1" ID doesn\'t exist.', $id));
        }
        return $store;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->storeCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $searchResults->setItems($collection->getData());
        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function delete(Data\StoreInterface $store): bool
    {
        try {
            $this->resource->delete($store);
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->getById($id));
    }
}
