<?php

namespace Elogic\Lesson\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Store extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('elogic_lesson_store', 'id');
    }
}
