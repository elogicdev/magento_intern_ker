<?php

namespace Elogic\Lesson\Model\ResourceModel\Store;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Elogic\Lesson\Model\Store as StoreModel;
use Elogic\Lesson\Model\ResourceModel\Store as StoreResourceModel;


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(StoreModel::class, StoreResourceModel::class);
    }
}
