<?php

declare(strict_types=1);

namespace Elogic\Lesson\Model;

use Elogic\Lesson\Model\ResourceModel\Store as StoreResourceModel;
use Elogic\Lesson\Api\Data\StoreInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

class Store extends AbstractModel implements StoreInterface
{
    /**
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(StoreResourceModel::class);
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return (int) $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function getName():string
    {
        return (string) $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function getImage(): ?string
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @inheritDoc
     */
    public function getAddress(): string
    {
        return (string) $this->getData(self::ADDRESS);
    }

    /**
     * @inheritDoc
     */
    public function getSchedule(): ?string
    {
        return $this->getData(self::SCHEDULE );
    }

    /**
     * @inheritDoc
     */
    public function getLongitude(): ?float
    {
        return (float) $this->getData(self::LONGITUDE);
    }

    /**
     * @inheritDoc
     */
    public function getLatitude(): ?float
    {
        return (float) $this->getData(self::LATITUDE);
    }

    /**
     * @inheritDoc
     */
    public function getUrlKey():string
    {
        return (string) $this->getData(self::URL_KEY);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId(int $id): StoreInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): StoreInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function setDescription(string $description = null): StoreInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function setImage(string $image = null): StoreInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @inheritDoc
     */
    public function setAddress($address): StoreInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @inheritDoc
     */
    public function setSchedule(string $schedule = null): StoreInterface
    {
        return $this->setData(self::SCHEDULE, $schedule);
    }

    /**
     * @inheritDoc
     */
    public function setLongitude(float $longitude = null): StoreInterface
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @inheritDoc
     */
    public function setLatitude(float $latitude = null): StoreInterface
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * @inheritDoc
     */
    public function setUrlKey(string $url_key): StoreInterface
    {
        return $this->setData(self::URL_KEY, $url_key);
    }

}
