<?php
declare(strict_types=1);

namespace Elogic\Lesson\Api\Data;

/**
 * Store interface.
 * @api
 * @since 100.0.2
 */
interface StoreInterface
{
    const ID          = 'id';
    const NAME        = 'name';
    const DESCRIPTION = 'description';
    const IMAGE       = 'image';
    const ADDRESS     = 'address';
    const SCHEDULE    = 'schedule';
    const LONGITUDE   = 'longitude';
    const LATITUDE    = 'latitude';
    const URL_KEY    = 'url_key';

    /**
     * @return int
     */
    public function getStoreId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @return string|null
     */
    public function getSchedule(): ?string;

    /**
     * @return float|null
     */
    public function getLongitude(): ?float;

    /**
     * @return float|null
     */
    public function getLatitude(): ?float;

    /**
     * @return string
     */
    public function getUrlKey(): string;

    /**
     * @param int $id
     * @return StoreInterface
     */
    public function setStoreId(int $id): self;

    /**
     * @param string $name
     * @return StoreInterface
     */
    public function setName(string $name): self;

    /**
     * @param string|null $description
     * @return StoreInterface
     */
    public function setDescription(string $description = null): self;

    /**
     * @param string|null $image
     * @return StoreInterface
     */
    public function setImage(string $image = null): self;

    /**
     * @param string $address
     * @return StoreInterface
     */
    public function setAddress(string $address): self;

    /**
     * @param string|null $schedule
     * @return StoreInterface
     */
    public function setSchedule(string $schedule = null): self;

    /**
     * @param float|null $longitude
     * @return StoreInterface
     */
    public function setLongitude(float $longitude = null): self;

    /**
     * @param float|null $latitude
     * @return StoreInterface
     */
    public function setLatitude(float $latitude = null): self;

    /**
     * @param string $url_key
     * @return StoreInterface
     */
    public function setUrlKey(string $url_key): self;
}
