<?php

declare(strict_types=1);

namespace Elogic\Lesson\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for store search results.
 * @api
 * @since 100.0.2
 */
interface StoreSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get stores list.
     *
     * @return StoreInterface[]
     */
    public function getItems();

    /**
     * Set stores list.
     *
     * @param StoreInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
