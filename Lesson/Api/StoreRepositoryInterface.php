<?php

declare(strict_types=1);

namespace Elogic\Lesson\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Elogic\Lesson\Api\Data\StoreInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use Elogic\Lesson\Api\Data\StoreSearchResultsInterface;

/**
 * Store CRUD interface.
 * @api
 * @since 100.0.2
 */
interface StoreRepositoryInterface
{
    /**
     * @param StoreInterface $store
     * @return StoreInterface
     * @throws LocalizedException
     */
    public function save(StoreInterface $store): StoreInterface;

    /**
     * @param int $id
     * @return StoreInterface
     * @throws LocalizedException
     */
    public function getById(int $id): StoreInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param StoreInterface $store
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(StoreInterface $store): bool;

    /**
     * @param int $id
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById(int $id): bool;
}
